﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlowOrb : MonoBehaviour
{
    public float spring = 5f;
    public float damping = 0.2f;

    Vector3 targetOffset;
    Transform parentTransform;
    [System.NonSerialized] public Vector3 velocity;
    

    // Start is called before the first frame update
    void Start()
    {
        spring *= Random.Range(0.8f, 1.2f);
        damping *= Random.Range(0.8f, 1.2f);
        var boss = GetComponentInParent<BossAgent>();
        parentTransform = boss ? boss.transform : transform.parent;
        targetOffset = parentTransform.InverseTransformPoint(transform.position);
        transform.parent = null;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (Time.fixedDeltaTime == 0)
            return; //Shouldn't happen?

        Vector3 targetPosition = parentTransform.TransformPoint(targetOffset);

        Vector3 dampingForce = -velocity * damping;
        Vector3 springForce = (targetPosition - transform.position) * spring;
        velocity += (dampingForce + springForce) * Time.fixedDeltaTime;
        transform.position += velocity * Time.fixedDeltaTime;

        transform.LookAt(Camera.main.transform.position);
    }
}
