﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossArm : MonoBehaviour
{
    public float hurtFlashDuration = 1.0f;
    public int hp = 4;

    float hurtFlashTimeLeft;
    MeshRenderer[] limbRenderers;
    List<Material> originalMaterials = new List<Material>();
    List<Material> hurtMaterials = new List<Material>();

    // Start is called before the first frame update
    void Start()
    {
        limbRenderers = GetComponentsInChildren<MeshRenderer>();
        foreach(var limbRenderer in limbRenderers)
        {
            originalMaterials.Add(limbRenderer.material);
            hurtMaterials.Add(new Material(limbRenderer.material));
        }
    }

    // Update is called once per frame
    void Update()
    {
        //if (Input.GetKeyDown(KeyCode.Space) && Random.value > 0.5f)
        //    Damage();

        if(hp <= 0)
        {
            for (int i = 0; i < limbRenderers.Length; i++)
            {
                hurtMaterials[i].color = Color.Lerp(hurtMaterials[i].color, new Color(0.1f, 0.1f, 0.1f, 0.75f), 0.1f);
                Color emissive = hurtMaterials[i].GetColor("_EmissionColor");
                emissive = Color.Lerp(emissive, new Color(0.13f, 0.24f, 0.24f), 0.1f);
                hurtMaterials[i].SetColor("_EmissionColor", emissive);
            }
            hurtFlashTimeLeft = 0;
        }
        if(hurtFlashTimeLeft > 0)
        {
            hurtFlashTimeLeft -= Time.deltaTime;
            for(int i = 0; i < limbRenderers.Length; i++)
            {
                var emissive = hurtMaterials[i].GetColor("_EmissionColor");
                emissive = Color.Lerp(Color.red, originalMaterials[i].GetColor("_EmissionColor"), 1 - hurtFlashTimeLeft / hurtFlashDuration);
                hurtMaterials[i].SetColor("_EmissionColor", emissive);
            }

            if(hurtFlashTimeLeft <= 0)
            {
                for (int i = 0; i < limbRenderers.Length; i++)
                {
                    limbRenderers[i].material = originalMaterials[i];
                }
            }
        }
    }

    public void Damage(int damage = 1)
    {
        var bossAgent = GetComponentInParent<BossAgent>();
        if(bossAgent && bossAgent.vulnerableMainbody)
        {
            bossAgent.TakeDamage(damage);
        }

        if (hp <= 0)
            return;

        hurtFlashTimeLeft = hurtFlashDuration;
        for (int i = 0; i < limbRenderers.Length; i++)
        {
            limbRenderers[i].material = hurtMaterials[i];
        }

        hp -= damage;
        if(hp <= 0)
        {
            Die();
        }
    }

    public void Die()
    {
        hp = 0;
        for (int i = 0; i < limbRenderers.Length; i++)
        {
            limbRenderers[i].material = hurtMaterials[i];
        }
    }
}
