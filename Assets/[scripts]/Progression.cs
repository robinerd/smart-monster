﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Progression : MonoBehaviour
{
    public GameObject minibossPrefab;
    public int waves = 2;
    public float maxWaveDuration = 30;
    public GameObject biggestBoss;
    public Transform[] portals;
    public Dictionary<Transform, BossAgent> portalMonsters = new Dictionary<Transform, BossAgent>();

    int wavesSpawned = 0;
    private float timeToNextSpawn = 6;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (wavesSpawned >= waves)
            return;

        if(timeToNextSpawn > 0)
        {
            timeToNextSpawn -= Time.deltaTime;
            if (timeToNextSpawn < 0)
            {
                timeToNextSpawn = maxWaveDuration;
                SpawnWave();
            }
        }
    }

    void SpawnWave()
    {
        wavesSpawned++;
        if (wavesSpawned == waves)
        {
            //Last wave spawns "mother"
            biggestBoss.SetActive(true);
        }
        else
        {
            foreach (Transform portal in portals)
            {
                GameObject miniboss = Instantiate(minibossPrefab, portal.position, Quaternion.identity);
            }
        }
    }
}
