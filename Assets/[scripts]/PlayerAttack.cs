﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour
{

    // This is where the arrows are created
    private Transform arrowSpawnPoint;
    public Arrow arrowPrefab;

    // Time between shots
    public float coolDown = 1.0f;
    private float timeLeftOfCoolDown;

    // For keeping track of arrows, so we can delete them;
    private List<Arrow> arrowsAlive;
    private int maxArrowsAlive = 100;

    // Reference to the weapon model so we can hide it.
    MeshRenderer arrowMeshRenderer;

    // Start is called before the first frame update
    void Start()
    {
        //arrowSpawnPoint = transform.Find("ProjectileSpawnPoint");
        arrowSpawnPoint = transform.GetComponentInChildren<ProjectileSpawn>().transform;
        arrowsAlive = new List<Arrow>();

        Transform arrowModel = transform.Find("FirstPersonCharacter/ProjectileSpawnPoint/ArrowModel");
        arrowMeshRenderer = arrowModel.GetComponentInChildren<MeshRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        timeLeftOfCoolDown = Mathf.MoveTowards(timeLeftOfCoolDown, 0.0f, Time.deltaTime);
        if(Input.GetMouseButton(0))
        {
            Shoot();
        }
        //if (Input.GetMouseButtonDown(1))
        //{
        //    Debug.Break();
        //}
        ShowHideArrow();
    }

    void Shoot()
    {
        if (timeLeftOfCoolDown <= 0f)
        {
            GameObject obj = Instantiate(arrowPrefab.gameObject, arrowSpawnPoint.position, arrowSpawnPoint.rotation);
            StoreArrow(obj.GetComponent<Arrow>());
            timeLeftOfCoolDown = coolDown;
        }
    }

    void StoreArrow(Arrow arrow)
    {
        if (arrowsAlive.Count >= maxArrowsAlive)
        {
            Arrow toDelete = arrowsAlive[0];
            Destroy(toDelete.gameObject);
            arrowsAlive.RemoveAt(0);
        }
        arrowsAlive.Add(arrow);
    }


    void ShowHideArrow()
    {
        if(arrowMeshRenderer.enabled && timeLeftOfCoolDown > 0)
        {
            arrowMeshRenderer.enabled = false;
        }
        else if (!arrowMeshRenderer.enabled && timeLeftOfCoolDown <= 0)
        {
            arrowMeshRenderer.enabled = true;
        }
    }
}
