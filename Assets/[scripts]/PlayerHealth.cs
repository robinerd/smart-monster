﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour
{
    public Image redFlash;
    public int maxHealth = 50;
    public GameObject deathScreen;

    public Slider healthSlider;

    private int health;


    private void Start()
    {
        health = maxHealth;
    }

    private void FixedUpdate()
    {
        if (redFlash)
            redFlash.color = Color.Lerp(redFlash.color, new Color(1, 0, 0, 0), 0.15f);
    }

    void OnDeath()
    {

    }

    public void TakeDamage(int damage = 5)
    {
        if (health <= 0) // Already dead
            return;

        health -= damage;

        UpdateUI();

        if(health <= 0)
        {
            Die();
        }
    }

    private void UpdateUI()
    {
        if(redFlash)
            redFlash.color = new Color(1.0f, 0, 0, 0.5f);

        healthSlider.value = healthSlider.maxValue * health / maxHealth;
    }

    public void Die()
    {
        health = 0;

        var fpsInput = GetComponentInParent<FirstPersonInput>();
        fpsInput.enabled = false;
        fpsInput.GetComponent<PlayerAttack>().enabled = false;
        //Camera.main.transform.GetChild(0).gameObject.SetActive(false);

        if (deathScreen)
            deathScreen.SetActive(true);
    }
}
