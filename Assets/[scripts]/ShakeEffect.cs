﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShakeEffect : MonoBehaviour
{
    public float lerpFactor;
    public float radius;
    Vector3 midPos;

    void Start()
    {
        midPos = transform.position;
    }
    
    void FixedUpdate()
    {
        Vector3 target = midPos + (Vector3)Random.insideUnitCircle * radius;
        transform.position = Vector3.Lerp(transform.position, target, lerpFactor);
    }
}
