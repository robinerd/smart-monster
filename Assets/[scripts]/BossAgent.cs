﻿using MLAgents;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class BossAgent : Agent
{
    public float maxMotorForce;
    public float targetMotorSpeed;
    public bool goCrazyOnDeath;
    public bool vulnerableMainbody = true;
    public int hp = 5;
    public float aboveGroundReward = 0.01f;
    public float inputScaleFactor = 1f;
    public Transform target;
    private Vector3 targetStartPos;
    private int groundLayerMask;
    private Rigidbody mainBody;
    public Rigidbody[] bodies { get; private set; }
    public HingeJoint[] muscles { get; private set; }
    private float latestReward;
    private bool crazyMode;
    [System.NonSerialized] public bool ignoreDecisions;
    BossArm[] bossArms;
    bool dead = false;

    private void Awake()
    {
        if (target == null)
            target = FirstPersonInput.player.transform;
    }

    // Start is called before the first frame update
    void Start()
    {
        bossArms = GetComponentsInChildren<BossArm>();
    }

    public override void CollectObservations()
    {
        Vector3 targetDirection = (target.position - mainBody.position).normalized;
        AddVectorObs(targetDirection);

        //Quaternion rotationToTarget = Quaternion.FromToRotation(Vector3.forward, targetDirection);

        foreach (var body in bodies)
        {
            AddVectorObs(body.velocity / 20.0f / inputScaleFactor);

            Vector3 vectorToBody = body.position - mainBody.position;

            // Global offset to mainbody
            AddVectorObs(vectorToBody / 20.0f / inputScaleFactor);

            //// Rotated in relation to target direction
            //AddVectorObs(rotationToTarget * vectorToBody / 15.0f);

            //// Local offset in main body
            //AddVectorObs(mainBody.transform.InverseTransformPoint(body.position) / 5.0f);

            float distanceToGround = raypickDistance(body.position, Vector3.down);
            AddVectorObs(distanceToGround);
        }

        foreach (var muscle in muscles)
        {
            AddVectorObs(muscle.angle / 360.0f);
            AddVectorObs(muscle.velocity / 400.0f);
            //AddVectorObs(muscle.motor.force / maxMotorForce);
        }
    }

    public override void AgentAction(float[] vectorAction, string textAction)
    {
        if (ignoreDecisions)
            return;

        // HANDLE ACTIONS
        int actionIndex = 0;

        foreach(var muscle in muscles)
        {
            float torqueAction = vectorAction[actionIndex++];

            // no force if dead
            if (muscle.GetComponentInParent<BossArm>().hp <= 0)
            {
                torqueAction = 0;
            }

            if (Mathf.Abs(torqueAction) > 0.05f)
            {
                var motor = muscle.motor;
                motor.force = Mathf.Clamp01(Mathf.Abs(torqueAction)) * maxMotorForce;
                motor.targetVelocity = Mathf.Sign(torqueAction) * targetMotorSpeed;
                muscle.motor = motor;
                muscle.useMotor = true;
            }
            else
            {
                muscle.useMotor = false;
            }
        }
        if (actionIndex != brain.brainParameters.vectorActionSize[0])
            Debug.LogError("Wrong action count! Should be " + brain.brainParameters.vectorActionSize[0] + ", got " + actionIndex);

        // ADD REWARDS
        if (target.gameObject.layer != LayerMask.NameToLayer("Player"))
        {
            float aboveGround = raypickDistance(mainBody.position, Vector3.down);
            AddReward(aboveGroundReward * aboveGround);

            Vector3 vectorToTarget = target.position - mainBody.position;
            Vector3 directionToTarget = vectorToTarget.normalized;
            if (mainBody.velocity.magnitude > 0)
            {
                float speedTowardsTarget = Vector3.Dot(mainBody.velocity, directionToTarget);
                AddReward(Mathf.Sign(speedTowardsTarget) * Mathf.Pow(Mathf.Abs(speedTowardsTarget) / 10.0f, 1.3f));
            }

            if (vectorToTarget.magnitude < 10)
            {
                AddReward(1);
                Vector2 randomOffset = Random.insideUnitCircle * 180; // Make sure it fits in level!
                target.position = new Vector3(
                    targetStartPos.x + randomOffset.x,
                    targetStartPos.y,
                    targetStartPos.z + randomOffset.y);
            }
        }

        latestReward = GetReward();
    }

    private float raypickDistance(Vector3 position, Vector3 direction)
    {
        float maxDistance = 20.0f;
        Ray ray = new Ray(position, direction);
        RaycastHit hitInfo;
        if (Physics.Raycast(ray, out hitInfo, maxDistance, groundLayerMask))
            return hitInfo.distance / maxDistance;

        return 1;
    }

    public override void InitializeAgent()
    {
        targetStartPos = target.position;
        groundLayerMask = LayerMask.GetMask(new string[] { "Default" });
        mainBody = GetComponent<Rigidbody>();
        bodies = GetComponentsInChildren<Rigidbody>();
        muscles = GetComponentsInChildren<HingeJoint>();
    }

    public override void AgentReset()
    {
        
    }

    private void FixedUpdate()
    {
        if(!dead)
        {
            bool diedNow = true;
            foreach(var arm in bossArms)
            {
                if (arm.hp > 0)
                {
                    diedNow = false;
                    break;
                }
            }

            if(diedNow)
            {
                allArmsDead();
            }
        }
    }

    public void OnDrawGizmos()
    {
        if (mainBody)
        {
            Gizmos.color = latestReward > 0 ? Color.green : Color.red;
            Gizmos.DrawSphere(mainBody.position + Vector3.up * 18, Mathf.Abs(latestReward) * 5);
        }
    }

    void allArmsDead()
    {
        agentParameters.onDemandDecision = true;

        if(goCrazyOnDeath && !crazyMode)
        {
            crazyMode = true;
            vulnerableMainbody = true;

            mainBody.drag = 0.15f;

            var cheatForce = GetComponent<BossCheatForce>();
            if (cheatForce)
                cheatForce.force *= 0.6f;

            var bossAttack = GetComponent<BossAttack>();
            if (bossAttack)
                bossAttack.launchForce *= 0.5f;

            DetachArms();
        }
    }

    public void TakeDamage(int damage)
    {
        if (!vulnerableMainbody)
            return;

        if (hp > 0)
        {
            hp -= damage;
            if (hp <= 0)
                Die();
        }

    }

    void DetachArms()
    {
        foreach (var arm in bossArms)
        {
            //Detach arms
            Destroy(arm.GetComponent<HingeJoint>());
            arm.transform.parent = null;
            arm.Damage(1000);
        }
    }

    void Die()
    {
        agentParameters.onDemandDecision = true;

        //Destroy(gameObject);
        var cheatForce = GetComponent<BossCheatForce>();
        if (cheatForce)
            Destroy(cheatForce);

        var bossAttack = GetComponent<BossAttack>();
        if (bossAttack)
        {
            foreach(var orb in bossAttack.orbs)
            {
                orb.transform.GetChild(0).gameObject.SetActive(false); //Disable glow
                orb.spring = 0;
                orb.damping = 0;
            }
            Destroy(bossAttack);
        }

        DetachArms();
    }
}
