﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossCheatForce : MonoBehaviour
{
    public float force = 200;
    Transform target;
    private Rigidbody body;

    // Start is called before the first frame update
    void Start()
    {
        target = GetComponent<BossAgent>().target;
        body = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        var towardsPlayer = target.position - body.position;
        towardsPlayer.y = 0;
        towardsPlayer.Normalize();

        float bodyRadius = body.GetComponent<Collider>().bounds.extents.x;
        body.AddForceAtPosition(towardsPlayer * force, body.position + Vector3.up * bodyRadius * 1.3f);
    }
}
