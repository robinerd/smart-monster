﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossAttack : MonoBehaviour
{
    public float launchForce = 1800;
    public float launchDuration = 0.2f;
    public float launchInterval = 6;
    public float chargeDuration = 1.7f;
    public float megaDrag = 15f;
    public float orbCharge = 600.0f;
    public AudioSource attackSound;

    public Transform target;
    private float launchTimeLeft;
    private Rigidbody body;
    private float launchCooldown;
    private float originalDrag;
    private float chargeTimeLeft;
    private BossAgent bossAgent;
    public GlowOrb[] orbs { get; private set; }

    // Start is called before the first frame update
    void Awake()
    {
        body = GetComponent<Rigidbody>();
        orbs = GetComponentsInChildren<GlowOrb>();

        launchCooldown = launchInterval;
        originalDrag = body.drag;
        bossAgent = GetComponent<BossAgent>();
    }

    void Start()
    {
        if (target == null)
            target = FirstPersonInput.player.transform;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(chargeTimeLeft > 0)
        {
            foreach (var orb in orbs)
                orb.velocity += Random.insideUnitSphere * orbCharge * Time.fixedDeltaTime;

            chargeTimeLeft -= Time.fixedDeltaTime;
            body.AddForce(Vector3.down * 80, ForceMode.Acceleration);
            if(chargeTimeLeft < 0)
            {
                //Debug.Log("LAUNCH");
                launchTimeLeft = launchDuration;
                body.drag = megaDrag;
                Invoke("RestoreDrag", launchTimeLeft + 1.0f);
            }
        }
        if(launchTimeLeft > 0)
        {
            launchTimeLeft -= Time.fixedDeltaTime;
            Vector3 toPlayer = (target.position - body.position).normalized;
            Vector3 chargeDirection = toPlayer + Vector3.up * 0.65f;
            chargeDirection.Normalize();
            body.AddForce(chargeDirection * launchForce, ForceMode.Acceleration);

            if(launchTimeLeft <= 0)
            {
                launchCooldown = Random.Range(0.9f, 1.1f) * launchInterval;
                //bossAgent.ignoreDecisions = false;
            }
        }
        else if(launchCooldown > 0)
        {
            launchCooldown -= Time.fixedDeltaTime;
            if(launchCooldown < 0)
            {
                chargeTimeLeft = chargeDuration;
                if (attackSound != null)
                    attackSound.Play();

                //bossAgent.ignoreDecisions = true;
                //foreach(var muscle in bossAgent.muscles)
                //{
                //    muscle.useMotor = false;
                //}
            }
        }
    }

    void RestoreDrag()
    {
        body.drag = originalDrag;
    }
}
