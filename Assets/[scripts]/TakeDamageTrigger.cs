﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TakeDamageTrigger : MonoBehaviour
{
    public int damage = 5;
    public float invulnerableTime = 1.0f;

    float lastDamagedTime;
    int monsterLayer;
    private PlayerHealth health;

    private void Start()
    {
        monsterLayer = LayerMask.NameToLayer("Monster");
        health = GetComponentInParent<PlayerHealth>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (Time.time > lastDamagedTime + invulnerableTime)
        {
            if (other.gameObject.layer == monsterLayer)
            {
                int damageToTake = damage;
                var arm = other.transform.GetComponentInParent<BossArm>();
                if (arm && arm.hp <= 0)
                    damageToTake /= 2;

                var bossagent = other.transform.GetComponentInParent<BossAgent>();
                if (bossagent && bossagent.hp <= 0)
                    return;

                health.TakeDamage(damageToTake);
                lastDamagedTime = Time.time;
            }
        }
    }
}
