﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : MonoBehaviour
{
    public float initialSpeed = 10;

    private Rigidbody rb;


    void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb.velocity = transform.forward * initialSpeed;
    }
    
    void Update()
    {
        
    }

    private void FixedUpdate()
    {
        SetRotationToDirection();
    }

    private void SetRotationToDirection()
    {
        // Make the arrow point in the direction it is going, if it is going fast.
        if(rb.velocity.magnitude > 6)
        {
            rb.rotation = Quaternion.LookRotation(rb.velocity, Vector3.up);
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        var bossArm = other.GetComponentInParent<BossArm>();
        if (bossArm != null)
            bossArm.Damage();
        else
        {
            var bossAgent = other.GetComponentInParent<BossAgent>();
            if (bossAgent != null)
                bossAgent.TakeDamage(1);
        }

        rb.isKinematic = true;
        rb.velocity = Vector3.zero;
        transform.SetParent(other.transform);

        GetComponent<Collider>().enabled = false;
    }
}
