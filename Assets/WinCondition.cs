﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinCondition : MonoBehaviour
{
    public BossAgent lastBoss;
    public GameObject winScreen;

    public void Update()
    {
        if (lastBoss.hp <= 0)
        {
            winScreen.SetActive(true);
            enabled = false;
        }
    }
}
