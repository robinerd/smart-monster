﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;
using UnityEngine.UI;

public class PlayerStamina : MonoBehaviour
{
    // How long the player can run.
    public float maxStaminaSeconds = 3;
    // How many seconds of stamina that is recovered per second when not running.
    public float regainRate = 0.5f;
    // After the player has depleted stamina, running will be disabled until stamina has replenished to this level (between 0 and 1);
    public float reenableStaminaThreshhold = 0.5f;

    public Slider staminaSlider;
    public Color goodStaminaColor;
    //public Color lowStaminaColor;
    public Color disabledStaminaColor;

    private float currentStamina;

    //Taken from FirstPersonCOntroller.cs. Is hardcoded.
    private KeyCode runButton = KeyCode.LeftShift;
    private FirstPersonInput firstPersonInput;

    private bool runningEnabled;

    // Image object of the slider, so we can change the color of it.
    private Image sliderImage;

    void Start()
    {
        firstPersonInput = GetComponent<FirstPersonInput>();
        currentStamina = maxStaminaSeconds;
        sliderImage = staminaSlider.transform.Find("Fill Area/Fill").GetComponent<Image>();
    }
    
    void Update()
    {
        UpdateCurrentStamina();
        EnableDisableRunning();
        UpdateUI();
    }

    private void UpdateCurrentStamina()
    {
        if (firstPersonInput.IsRunning() && runningEnabled)
        {
            currentStamina = Mathf.MoveTowards(currentStamina, 0f, Time.deltaTime);
        }
        else
        {
            currentStamina = Mathf.MoveTowards(currentStamina, maxStaminaSeconds, Time.deltaTime * regainRate);
        }
    }

    private void EnableDisableRunning()
    {
        if(currentStamina <= 0f)
        {
            runningEnabled = false;
        } 
        else if(currentStamina > reenableStaminaThreshhold*maxStaminaSeconds)
        {
            runningEnabled = true;
        }
        firstPersonInput.EnableRunning(runningEnabled);

    }

    private void UpdateUI()
    {
        staminaSlider.value = staminaSlider.maxValue * currentStamina / maxStaminaSeconds;

        if(runningEnabled)
        {
            sliderImage.color = goodStaminaColor;
        }
        else
        {
            sliderImage.color = disabledStaminaColor;
        }

        // Hide stamina bar if we are at full stamina
        staminaSlider.gameObject.SetActive(currentStamina != maxStaminaSeconds);
    }
}
