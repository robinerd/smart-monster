﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MLAgents;

public class BasicAcademy : Academy {

    public static bool isTraining = false;

    public override void AcademyReset()
    {
        isTraining = !GetIsInference();
    }

    public override void AcademyStep()
    {

    }

    public override void InitializeAcademy()
    {
    }
}
